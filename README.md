# Plausible tracking 
This module integrates Plausible Analytics which is a simple, open source, lightweight and privacy-friendly alternative to Google Analytics. 

## How to install
Use compser to install the module like so:  
 `composer require 'drupal/plausible_tracking:[VERSION]'`  
Use drush to enable the module:  
`drush en plausible_tracking`

## How to configure
You can access the config form on:   
`/admin/config/plausible`  
You'll need to enter the domain name of your site. **Don't include https://**  
The rest of the config is optional. 

## Setup with docker and platformsh
It's recommended to change domain depending on environment. Typically you have one domain for dev and another for prod. 

### Add the dev domain
In the docker.settings.php file add this and replace 'yoursite.docker' with your actual domain:  
`$config['plausible_tracking.settings']['domain'] = 'yoursite.docker';`

### Add the prod domain
In the platformsh.env.main.settings.php file add this and replace 'yoursite.com' with your actual domain:    
`  $config['plausible_tracking.settings']['domain'] = 'yoursite.com';` 

### Block ips on prod
It's recommended to add the blocked ips in the platformsh.env.main.settings.php file:  
`$config['plausible_tracking.settings']['blocked_ips'] = [
    '000.000.000.00',
    '00.000.000.0',
  ];`   

## How to track event goal
To track events we use special data attributes on the element we want to track:

### Trigger
`data-plausible-trigger` which specifies the event that will be listend to:
  * `click` - fires on click
  * `in-view` - fires when element is in view
  * `not-in-view` - fires when element is not in view 

### Id
 `data-plausible-id` is the name of the event goal you track. This will be displayed as the title in the plausible dashboard.
 
### Props
`data-plausible-props` is a custom properties object with key value pairs that can collect data that Plausible doesn't automatically track.

### Example of click event
Say you have two buttons and you want to track their value each time they're clicked. To do this you add some special data attribute to the buttons:  
 * We'll listen to clicks: `data-plausible-trigger=click`
 * We use `data-plausible-id="Button clicks"`
 * We track the key **answer** and the value **Yes/no**  

        <button
          data-plausible-trigger="click"
          data-plausible-id="Button clicks"
          data-plausible-props='{ "answer": "Yes" }'
          >
          Yes
          </button>

        <button
          data-plausible-trigger="click"
          data-plausible-id="Button clicks"
          data-plausible-props='{ "answer": "No" }'
          >
          No
          </button>

To see the result in the plausible dashboard you'll have to add the event id. Check their guide [here](https://plausible.io/docs/custom-event-goals#3-create-a-custom-event-goal-in-your-plausible-account)

The result in the plausible dashboard:  

![plausible custom props example](https://i.imgur.com/2UoQ0kB.png)
