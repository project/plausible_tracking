/**
 * @file
 * Activating plausible tracking, if the current user IP is allowed.
 */

Drupal.behaviors.plausible_init = {
  // eslint-disable-next-line no-unused-vars
  attach: function (context, settings) {
    // Allow for Event goal tracking.
    // This line comes directly from Plausible docs.
    window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }
    const blockedIps = drupalSettings?.plausibleTracking?.blockedIps;

    // If no blocked IPs are set, activate the scripts.
    if ((!blockedIps?.length) || (blockedIps[0] === '')) {
      this.activateScripts();
      return;
    }

    var that = this;

    fetch('/plausible/ip_check')
      .then(function(response) {
        if (response.status === 200) {
          that.activateScripts();
        }
        else {
          console.log('Plausible script has not been loaded; IP blocked.')
        }
      })
      .catch(function() {
        console.error('Could not access /plausible/ip_check')
      });
  },

  activateScripts: function() {
    var inactiveScripts = document.querySelectorAll('[data-plausible-src]');

    inactiveScripts.forEach(function(script) {
      var src = script.getAttribute('data-plausible-src');

      script.setAttribute('src', src);
      script.removeAttribute('data-plausible-src');
    })
  }
}
