/**
 * @file
 * Tracking events/goals using Plausible.
 *
 * More info here:
 * https://plausible.io/docs/custom-event-goals
 */

Drupal.behaviors.plausible_events = {
  attach: function () {
    const that = this;

    document
      .querySelectorAll(
        "[data-plausible-id][data-plausible-trigger]:not(.is-plausible-initialized)"
      )
      .forEach((element) => {
        element.classList.add("is-plausible-initialized");
        that.plausibleTracking(element);
      });

    this.trackPageview();
  },

  plausibleTracking: function (element) {
    const elementAttributes = this.getElementAttributes(element);

    window.plausible_trigger_count = [];

    var that = this;

    // This is possible if an empty data- is set without value.
    if (!elementAttributes.triggerListen || !elementAttributes.id) {
      return;
    }

    switch (elementAttributes.triggerListen) {
      case "in-view":
        that.inViewEvent(false, elementAttributes, element);
        break;

      case "not-in-view":
        that.inViewEvent(true, elementAttributes, element);
        break;

      // By default, the triggerListen amounts to the same you'll pass to
      // an eventListener - such as 'click'.
      default:
        element.addEventListener(
          elementAttributes.triggerListen,
          function (event) {
            that.triggerEvent(event, elementAttributes, element);
          }
        );
        break;
    }
  },

  triggerEvent: function (event, elementAttributes) {
    let triggerCount =
      window.plausible_trigger_count[elementAttributes.id] || 0;

    // On most elements, we only want to track one event per page view.
    if (triggerCount && elementAttributes.disallowMultipleTriggers) {
      return;
    }

    // If we have an element that contains a lot of the same elements which
    // we want to listen to (such as the buttons on each of the search results),
    // it's better that we have a single listener on the parent wrapper,
    // and on event trigger, we check if the element clicked/whatever is
    // relevant.
    if (
      elementAttributes.childrenSelectorClass &&
      !event.target.classList.contains(elementAttributes.childrenSelectorClass)
    ) {
      return;
    }

    this.pingPlausible(elementAttributes.id, elementAttributes.getProps());

    window.plausible_trigger_count[elementAttributes.id] =
      parseInt(triggerCount) + 1;
  },

  inViewEvent: function (negate = false, elementAttributes, element) {
    var that = this;

    // Only do stuff for browsers that support 'in-view'.
    if (!window.IntersectionObserver) {
      return;
    }

    // Creating the observer, which we'll place on the element.
    var observer = new IntersectionObserver(
      function (entries, observer) {
        entries.forEach(function (entry) {
          if (
            (!negate && entry.isIntersecting) ||
            (negate && !entry.isIntersecting)
          ) {
            that.pingPlausible(
              elementAttributes.id,
              elementAttributes.getProps()
            );

            // The item has come into view, so we'll stop observing.
            observer.unobserve(entry.target);
          }
        });
      },
      {rootMargin: "0px 0px 0px 0px"}
    );

    observer.observe(element);
  },

  pingPlausible: function (id, props, customUrl) {
    // If the props object is a string convert it to a json object.
    // The Plausible function argument expects a json object.
    if (typeof props == "string") {
      props = JSON.parse(props || "{}");
    }

    if (id === 'pageview') {
      plausible('pageview', {u: customUrl, props: props });
      return;
    }

    // eslint-disable-next-line no-undef
    plausible(id, {props: props});
  },

  getElementAttributes: function (element) {
    return {
      triggerListen: element.getAttribute("data-plausible-trigger"),
      id: element.getAttribute("data-plausible-id"),
      getProps: () => {
        const props = element.getAttribute("data-plausible-props");
        return props ? props : {};
      },
      disallowMultipleTriggers: element.hasAttribute(
        "data-plausible-disallow-multiple"
      ),
      childrenSelectorClass: element.getAttribute(
        "data-plausible-children-selector-class"
      ),
    };
  },

  trackPageview: function () {
    const {
      trackQueryParams,
      queryParams,
    } = drupalSettings?.plausibleTracking;

    // If query tracking is not enabled the manual script is not loaded.
    // Therefor we don't need to send pageviews manually.
    if (!trackQueryParams) {
      return;
    }

    // If there are query params to track, we ping plausible
    // with a pageview event goal with props and
    // custom url with query params.
    if (queryParams?.length) {
      const queryWasTracked = this.trackQueryParams(queryParams);

      if (queryWasTracked) {
        return;
      }
    }

    // If no query params were tracked, we ping plausible
    // with a pageview event goal without query params props
    // and only a base url.
    this.pingPlausible('pageview', null, `${location.origin}${location.pathname}`);
  },

  // We use pageview event goal to track url search params.
  // More info here: https://plausible.io/docs/custom-query-params
  // How to add pageview goals in plausible:
  // https://plausible.io/docs/pageview-goals
  trackQueryParams: function (queryParamsToTrack) {
    const { currentQuery } = drupalSettings?.path;

    // If the current url contains no query params.
    if (!currentQuery) {
      return false;
    }

    const url = new URL(location.href);
    let customUrl = `${url.protocol}//${url.hostname}${url.pathname.replace(/\/$/, '')}`;
    let props = {};

    // Add the params we want to track to the custom url and props.
    queryParamsToTrack.forEach((paramName) => {
      //const paramValue = queryParams.get(paramName)
      const paramValue = currentQuery[paramName];

      if (!paramValue) {
        return;
      }

      customUrl = `${customUrl}/${paramName}=${paramValue}`;
      props[paramName] = paramValue;
    });

    if (!Object.keys(props).length) {
      return false;
    }

    this.pingPlausible('pageview', props, customUrl);
    return true;
  },
};

Drupal.behaviors.plausible_events.attach();
