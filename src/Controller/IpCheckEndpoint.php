<?php

namespace Drupal\plausible_tracking\Controller;

use Drupal\plausible_tracking\Form\PlausibleConfigForm;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * An endpoint for checking if a client IP is OK to be Plausible tracked.
 */
class IpCheckEndpoint extends ControllerBase {

  /**
   * The config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory
  ) {
    $this->config = $config_factory->get(PlausibleConfigForm::SETTINGS_CONFIG_ID);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * Informs if client IP is not allowed to be Plausible tracked.
   */
  public function endpoint(Request $request) {
    $ip = $request->getClientIp();
    $blocked_ips = $this->config->get('blocked_ips') ?? [];

    if (in_array($ip, $blocked_ips)) {
      return new Response(
        'blocked',
        Response::HTTP_FORBIDDEN,
        ['content-type' => 'text/html']
      );
    }

    return new Response(
      'ok',
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );
  }

}
