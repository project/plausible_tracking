<?php

namespace Drupal\plausible_tracking\Services;

use Drupal\plausible_tracking\Form\PlausibleConfigForm;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\State\State;
use Drupal\node\Entity\Node;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A service for calling the Plausible API(s).
 */
class PlausibleApi {

  /**
   * Config storage.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Our logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Unofficial API base url.
   *
   * This API is not documented in the Plausible docs.
   * Instead, it's the API used by Plausible.io to expose data for their
   * own dashboard.
   * This API has access to things that the official API does not - but
   * requires a cookie authentication, rather than bearer token.
   *
   * @var string
   */
  protected string $unofficialApiBaseUrl;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    State $state,
    RouteMatchInterface $route_match,
    Client $http_client,
    LoggerInterface $logger,
  ) {
    $this->config = $config_factory->get(PlausibleConfigForm::SETTINGS_CONFIG_ID);

    $domain = $this->config->get('domain');

    $this->state = $state;
    $this->routeMatch = $route_match;
    $this->httpClient = $http_client;
    $this->logger = $logger;

    $this->unofficialApiBaseUrl = "https://plausible.io/api/stats/{$domain}/";
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('current_route_match'),
      $container->get('http_client'),
      $container->get('logger.channel.plausible_tracking')
    );
  }

  /**
   * Calling the unofficial API endpoint.
   *
   * See the documentation for the $unofficialApiBaseUrl property.
   */
  private function getUnofficialApiResponse($endpoint, $filters = [], $period = '30d') {
    $cookie_key = $this->state->get(PlausibleConfigForm::COOKIE_KEY_STATE_ID);

    if (empty($this->config->get('enable_unofficial_api')) || empty($cookie_key)) {
      $this->logger->error("Cannot call unofficial API; Either missing cookie key or setting not enabled.");

      return FALSE;
    }

    $client = $this->httpClient;
    $endpoint_url = "{$this->unofficialApiBaseUrl}{$endpoint}";

    $cookie_jar = CookieJar::fromArray([
      '_plausible_key' => $cookie_key,
    ], 'plausible.io');

    $options = [
      'cookies' => $cookie_jar,
      'query' => [
        'filters' => json_encode($filters),
        'date' => date('Y-m-d'),
        'with_imported' => 'true',
        'period' => $period,
      ],
    ];

    try {
      $response = $client->request('get', $endpoint_url, $options);

      $data = $response->getBody()->getContents();
      $data = json_decode($data, TRUE) ?? $data;

      if (is_array($data) && count($data) < 2) {
        $data = reset($data);
      }

      return $data;
    }
    catch (\Exception $e) {
      $this->logger->error("Failed calling unofficial Plausible endpoint: {$endpoint}'");

      return FALSE;
    }
  }

  /**
   * Getting detailed visit + goal stats for a specific node.
   */
  public function getNodeStats(Node $node) {
    $uri = $node->toUrl()->toString();

    if (empty($uri)) {
      return NULL;
    }

    $results = $this->getPathStats($uri);

    return $results;
  }

  /**
   * Getting detailed visit + goal stats for a specific relative path.
   */
  public function getPathStats(string $uri) {
    $filters = [
      'page' => $uri,
    ];

    $goals = $this->getGoals($filters) ?? [];

    foreach ($goals as &$goal) {
      $prop_names = $goal['prop_names'] ?? [];

      foreach ($prop_names as $prop_name) {
        if (empty($prop_name)) {
          continue;
        }

        $prop_submissions = $this->getUnofficialApiResponse("property/{$prop_name}", $filters) ?? [];

        $goal['prop_submissions'] = $prop_submissions;
      }
    }

    return [
      'stats' => $this->getUnofficialApiResponse('pages', $filters) ?? [],
      'goals' => $goals ?? [],
    ];
  }

  /**
   * Get goal stats, across all pages.
   */
  public function getGoalPagesStats(string $goal_name, $props = []) {
    return $this->getUnofficialApiResponse(
      'pages',
      [
        'goal' => $goal_name,
        'props' => $props,
      ]
    ) ?? [];
  }

  /**
   * Get detailed details for a specific goal.
   *
   * This is necessary if you also want to pull out prop data.
   */
  public function getGoal(string $goal_name, array $filters = []) {
    $filters = $filters + ['goal' => $goal_name];

    return $this->getUnofficialApiResponse(
      'conversions',
      $filters
    ) ?? [];
  }

  /**
   * Get detailed list of active/relevant goals.
   */
  public function getGoals(array $filters = []) {
    $goals = $this->getUnofficialApiResponse(
      'conversions',
      $filters
    ) ?? [];
    $results = [];

    foreach ($goals as $goal) {
      $goal_name = $goal['name'] ?? NULL;

      if (!$goal_name) {
        continue;
      }

      // Load the detailed goal.
      $results[] = $this->getGoal($goal_name, $filters);
    }

    return $results;
  }

}
