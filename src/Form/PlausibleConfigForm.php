<?php

namespace Drupal\plausible_tracking\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A config form, for module related settings.
 */
class PlausibleConfigForm extends ConfigFormBase {

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $message;

  const COOKIE_KEY_STATE_ID = 'plausible_tracking.settings.cookie_key';
  const SETTINGS_CONFIG_ID = 'plausible_tracking.settings';
  const FORM_SETTINGS_PREFIX = 'setting_';

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): self {
    $form = parent::create($container);
    $form->state = $container->get('state');
    $form->message = $container->get('messenger');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'plausible_tracking_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS_CONFIG_ID,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_CONFIG_ID);
    $values = $form_state->getValues();
    $new_values = [];

    foreach ($values as $key => $value) {
      if (!str_starts_with($key, self::FORM_SETTINGS_PREFIX)) {
        continue;
      }

      // Removing the setting prefix, to not save it in the config.
      $key = substr($key, strlen(self::FORM_SETTINGS_PREFIX));
      $new_values[$key] = $value;
    }

    $blocked_ips = $new_values['blocked_ips'] ?? '';
    $query_params = $new_values['query_params'] ?? '';

    // Remove whitespaces.
    $blocked_ips = preg_replace('/\s+/', '', $blocked_ips);
    $query_params = preg_replace('/\s+/', '', $query_params);

    // Turn a string of CSV into an array.
    $blocked_ips = explode(',', trim($blocked_ips));
    $query_params = explode(',', trim($query_params));

    $new_values['blocked_ips'] = $blocked_ips;
    $new_values['query_params'] = $query_params;

    foreach ($new_values as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();

    $cookie_key = $form_state->getValue('state_cookie_key') ?? NULL;
    $this->state->set(self::COOKIE_KEY_STATE_ID, $cookie_key);

    $this->message->addStatus($this->t('Successfully saved'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_CONFIG_ID);
    $blocked_ips = $config->get('blocked_ips');
    $query_params = $config->get('query_params');

    $prefix = self::FORM_SETTINGS_PREFIX;

    $form["{$prefix}domain"] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#description' => $this->t('The domain that Plausible has been set up with. Do NOT include http(s)://.'),
      '#attributes' => [
        'placeholder' => 'example.com',
      ],
      '#default_value' => $config->get('domain'),
    ];

    $form["{$prefix}admin_pages_enabled"] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Also include Plausible tracking on admin pages?'),
      '#default_value' => $config->get('admin_pages_enabled'),
    ];

    $form["{$prefix}blocked_ips"] = [
      '#type' => 'textarea',
      '#title' => $this->t('IPs that do NOT get Plausible script loaded. Seperate with commas.'),
      '#default_value' => empty($blocked_ips) ? '' : implode(',', $blocked_ips),
      '#rows' => 1,
    ];

    $form["{$prefix}track_downloads"] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track downloads'),
      '#default_value' => $config->get('track_downloads'),
      '#description' => $this->t(
        'If checked, the file-downloads script is added. Add a "File Download" event goal in plausible to see the results. <a href="@url" target="_blank">More info.</a>',
        ['@url' => 'https://plausible.io/docs/file-downloads-tracking'],
      ),
    ];

    $form["{$prefix}track_outbound_link_click"] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track outbound link clicks'),
      '#default_value' => $config->get('track_outbound_link_click'),
      '#description' => $this->t(
        'If checked, the outbound-links script is added. Add an "Outbound Link: Click" event goal in plausible to see the results. <a href="@url" target="_blank">More info.</a>',
        ['@url' => 'https://plausible.io/docs/outbound-link-click-tracking'],
      ),
    ];

    $query_params_group_form_key = "{$prefix}query_params_group";

    $form[$query_params_group_form_key] = [
      '#type' => 'fieldset',
      '#title' => t('Query param settings'),
    ];

    $track_query_params_form_key = "{$prefix}track_query_params";

    $form[$query_params_group_form_key][$track_query_params_form_key] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track query parameters'),
      '#default_value' => $config->get('track_query_params'),
      '#description' => $this->t(
        'If checked, the manual script is added and enables tracking of custom query parameters. Add a "pageview" goal in plausible to see the results.  <a href="@url" target="_blank">More info.</a>',
        ['@url' => 'https://plausible.io/docs/pageview-goals'],
      ),
    ];

    $form[$query_params_group_form_key]["{$prefix}query_params"] = [
      '#type' => 'textarea',
      '#title' => $this->t('The names of query params to track. Seperate with commas.'),
      '#description' => $this->t('Example: track the "q" query param from the url: "/search?q=test", by adding "q" to the list.'),
      '#default_value' => empty($query_params) ? '' : implode(',', $query_params),
      '#rows' => 1,
      '#states' => [
        'invisible' => [
          ':input[name="' . $track_query_params_form_key . '"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="' . $track_query_params_form_key . '"]' => ['checked' => TRUE],
        ],
      ],
    ];

    /*    $unofficial_api_form_key = "{$prefix}enable_unofficial_api";

    $form[$unofficial_api_form_key] = [
    '#type' => 'checkbox',
    '#title' => $this->t('(EXPERIMENTAL) Enable the expanded API service..'),
    '#default_value' => $config->get('enable_unofficial_api'),
    '#description' => $this->t(
    'This feature requires authentication through your plausible login cookie, and is therefore prone to instability.'
    ),
    ];

    // We want to save this value in state, rather than config.
    $form["state_cookie_key"] = [
    '#type' => 'textarea',
    '#title' => $this->t('Plausible.io cookie key'),
    '#default_value' => $this->state->get(self::COOKIE_KEY_STATE_ID),
    '#description' => $this->t(
    'Log in to plausible.io, and get the <strong>_plausible_key</strong> value from the browser cookies.<br> This cookie will expire after a certain time, and will need you to re-insert it.'
    ),
    '#rows' => 1,
    '#states' => [
    'invisible' => [
    ':input[name="' . $unofficial_api_form_key . '"]' => ['checked' => FALSE],
    ],
    'required' => [
    ':input[name="' . $unofficial_api_form_key . '"]' => ['checked' => TRUE],
    ],
    ],
    ];*/

    return parent::buildForm($form, $form_state);
  }

}
