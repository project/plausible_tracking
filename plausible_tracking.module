<?php

/**
 * @file
 * Plausible tracking.
 */

/**
 * Implements hook_page_attachments().
 *
 * Embed plausible scripts.
 */
function plausible_tracking_page_attachments(array &$attachments) {
  $config = \Drupal::config('plausible_tracking.settings');
  $domain = $config->get('domain');

  if (empty($domain) || !_plausible_tracking_allow_script()) {
    return;
  }

  $attachments['#attached']['library'][] = 'plausible_tracking/base';

  $attachments['#attached']['html_head'][] = [
    [
      '#tag' => 'script',
      '#attributes' => [
        'defer' => TRUE,
        'data-domain' => $domain,
        'data-plausible-src' => _get_plausible_script(),
      ],
    ],
    'plausible-tracking',
  ];

  $module_location = '/' . \Drupal::service('extension.list.module')
    ->getPath('plausible_tracking');

  $attachments['#attached']['html_head'][] = [
    [
      '#tag' => 'script',
      '#attributes' => [
        'defer' => TRUE,
        'data-plausible-src' => "$module_location/js/plausible-events.js",
      ],
    ],
    'plausible-tracking-events',
  ];

  $attachments['#attached']['drupalSettings']['plausibleTracking']['blockedIps'] =
    $config->get('blocked_ips');

  $attachments['#attached']['drupalSettings']['plausibleTracking']['trackQueryParams'] =
    $config->get('track_query_params');

  $attachments['#attached']['drupalSettings']['plausibleTracking']['queryParams'] =
    $config->get('query_params');
}

/**
 * Helper function to check if the script is allowed to be placed on this page.
 */
function _plausible_tracking_allow_script() {
  $admin_pages_enabled =
    \Drupal::config('plausible_tracking.settings')->get('admin_pages_enabled');

  // Checking if we're on an admin page, if the script has been set up
  // not to be applied on admin pages.
  if (empty($admin_pages_enabled) &&
    \Drupal::service('router.admin_context')->isAdminRoute()) {
    return FALSE;
  }

  // Checking if the current user has permissions to avoid tracking.
  if (\Drupal::currentUser()
    ->hasPermission('Plausible tracking do not track')) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Helper function to add extension scripts to the plausible script.
 */
function _get_plausible_script(): string {
  $config = \Drupal::config('plausible_tracking.settings');

  $track_downloads = $config->get('track_downloads');
  $track_query_params = $config->get('track_query_params');
  $track_outbound_link_click = $config->get('track_outbound_link_click');

  $extension_scripts = [];

  if ($track_downloads) {
    $extension_scripts[] = 'file-downloads';
  }

  if ($track_query_params) {
    $extension_scripts[] = 'manual';
  }

  if ($track_outbound_link_click) {
    $extension_scripts[] = 'outbound-links';
  }

  $extension_scripts_string = implode('.', $extension_scripts);

  return empty($extension_scripts) ?
    'https://plausible.io/js/script.js' :
    'https://plausible.io/js/script.' . $extension_scripts_string . '.js';
}
